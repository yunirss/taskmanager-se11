package ru.suyundukov.tm;

import jakarta.persistence.EntityManagerFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.reflections.Reflections;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.service.ProjectService;
import ru.suyundukov.tm.service.TaskService;
import ru.suyundukov.tm.service.UserService;
import ru.suyundukov.tm.utils.ConsoleHelper;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class Bootstrap implements ServiceLocator {
    private ProjectService projectService;
    private TaskService taskService;
    private UserService userService;
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private final Set<Class<? extends AbstractCommand>> commandClasses = getAbstractCommandClasses();

    private Set<Class<? extends AbstractCommand>> getAbstractCommandClasses() {
        return new Reflections("ru.suyundukov.tm").getSubTypesOf(AbstractCommand.class);
    }

    public void init() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        for (final Class<? extends AbstractCommand> abstractClass : commandClasses) {
            registry(abstractClass.getDeclaredConstructor().newInstance());
        }
    }

    private void registry(final AbstractCommand command) {
        final String commandName = command.getName();
        command.setServiceLocator(this);
        commands.put(commandName, command);
    }

    public void start() {
        try {
            projectService = new ProjectService(factory());
            taskService = new TaskService(factory());
            userService = new UserService(factory());
            init();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        System.out.println("WELCOME TO TASK MANAGER");
        while (true) {
            try {
                String string = ConsoleHelper.readString().trim();
                AbstractCommand command = null;
                for (AbstractCommand abstractCommand : getCommands()) {
                    if (abstractCommand.getName().equalsIgnoreCase(string)) {
                        if (userService.getUserOnline() == null) {
                            if (abstractCommand.isNeedAuthorization()) {
                                System.out.println("[YOU NEED LOG IN TO USE THIS COMMANDS]");
                                break;
                            }
                        }
                        command = abstractCommand;
                        command.setServiceLocator(this);
                        command.execute();
                        break;
                    }
                }
                if (command == null) {
                    System.out.println("[PLEASE, TYPE CORRECT COMMAND OR ENTER <<help>>]");
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public EntityManagerFactory factory() {
        final Properties property = new Properties();
        try {
            property.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("database.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        final Map<String, Object> settings = new HashMap<>();
        settings.put(Environment.DRIVER, property.getProperty("db.driver"));
        settings.put(Environment.URL, property.getProperty("db.host"));
        settings.put(Environment.USER, property.getProperty("db.login"));
        settings.put(Environment.PASS, property.getProperty("db.password"));
        settings.put(Environment.DIALECT,
                "org.hibernate.dialect.PostgreSQLDialect");
        settings.put(Environment.HBM2DDL_AUTO, "none");
        settings.put(Environment.SHOW_SQL, "true");
        final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @Override
    public UserService getUserService() {
        return userService;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }
}