package ru.suyundukov.tm.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public enum RoleType {
    ADMIN("admin", "description"),
    USER("user", "description");
    private final String name;
    private final String description;
}
