package ru.suyundukov.tm;

import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.service.ProjectService;
import ru.suyundukov.tm.service.TaskService;
import ru.suyundukov.tm.service.UserService;

import java.util.Collection;

public interface ServiceLocator {
    ProjectService getProjectService();

    TaskService getTaskService();

    UserService getUserService();

    Collection<AbstractCommand> getCommands();
}
