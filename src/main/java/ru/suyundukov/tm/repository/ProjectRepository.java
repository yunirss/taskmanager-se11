package ru.suyundukov.tm.repository;

import ru.suyundukov.tm.entity.Project;

import java.sql.SQLException;
import java.util.List;

public interface ProjectRepository {
    void save(Project project) throws SQLException;

    List<Project> findAll() throws SQLException;

    Project findOne(Integer id) throws SQLException;

    Project findByName(String name) throws Exception;

    List<Project> findProjectsByUserId(int userId) throws SQLException;

    void updateProject(Project project) throws Exception;

    void deleteById(Integer id) throws SQLException;

    void deleteByName(String name) throws Exception;
}
