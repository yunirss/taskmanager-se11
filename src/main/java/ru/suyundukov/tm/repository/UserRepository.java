package ru.suyundukov.tm.repository;

import ru.suyundukov.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface UserRepository {

    void save(User user) throws SQLException;

    List<User> findAll() throws SQLException;

    User findById(Integer id) throws SQLException;

    User findByName(String name) throws SQLException;

    void deleteById(Integer id) throws SQLException;

}

