package ru.suyundukov.tm.repository.impl;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import lombok.AllArgsConstructor;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.repository.UserRepository;

import java.sql.SQLException;
import java.util.List;

@AllArgsConstructor
public class UserRepositoryImpl implements UserRepository {
    private final EntityManager entityManager;

    @Override
    public void save(User user) throws SQLException {
        entityManager.persist(user);
    }

    @Override
    public List<User> findAll() throws SQLException {
        return entityManager.createQuery("select u from User u", User.class).getResultList();
    }

    @Override
    public User findById(Integer id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public User findByName(String name) throws SQLException {
        return entityManager.find(User.class, name);
    }

    @Override
    public void deleteById(Integer id) throws SQLException {
        entityManager.remove(findById(id));
    }
}
