package ru.suyundukov.tm.repository.impl;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import lombok.AllArgsConstructor;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.repository.TaskRepository;

import java.sql.SQLException;
import java.util.List;

@AllArgsConstructor
public class TaskRepositoryImpl implements TaskRepository {
    private final EntityManager entityManager;

    @Override
    public void save(Task task) throws SQLException {
        entityManager.persist(task);
    }

    @Override
    public List<Task> findAll() throws SQLException {
        return entityManager.createQuery("select t from Task t", Task.class).getResultList();
    }

    @Override
    public Task findOne(Integer id) {
        return entityManager.find(Task.class, id);
    }

    @Override
    public Task findByName(String name) throws SQLException {
        TypedQuery<Task> query = entityManager.createQuery("select t from Task t where t.name = :name", Task.class);
        query.setParameter("name", name);
        return query.getSingleResult();
    }

    @Override
    public List<Task> findTasksByProjectId(int projectId) {

        TypedQuery<Task> query = entityManager.createQuery("select t from Task t where t.projectId = :projectId", Task.class);
        query.setParameter("projectId", projectId);
        return query.getResultList();
    }

    @Override
    public List<Task> findTasksByUserId(int userId) {
        TypedQuery<Task> query = entityManager.createQuery("select t from Task t where t.userId = :userId", Task.class);
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    @Override
    public void deleteById(Integer id) throws SQLException {
        entityManager.remove(findOne(id));
    }
    @Override
    public void deleteTasksByProjectId(Integer id) {
        entityManager.remove(findTasksByProjectId(id));
    }

    @Override
    public void deleteByName(String name) throws SQLException {
        entityManager.remove(findByName(name));
    }

    @Override
    public void updateTask(Task task) {
        entityManager.merge(task);
    }
}
