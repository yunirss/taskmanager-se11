package ru.suyundukov.tm.repository.impl;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import lombok.AllArgsConstructor;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.repository.ProjectRepository;

import java.sql.SQLException;
import java.util.List;

@AllArgsConstructor
public class ProjectRepositoryImpl implements ProjectRepository {
    private final EntityManager entityManager;

    @Override
    public void save(Project project) throws SQLException {
        entityManager.persist(project);
    }

    @Override
    public List<Project> findAll() throws SQLException {
        return entityManager.createQuery("select p from Project p", Project.class).getResultList();
    }

    @Override
    public Project findOne(Integer id) {
        return entityManager.find(Project.class, id);
    }

    @Override
    public Project findByName(String name) throws Exception {
        TypedQuery<Project> query = entityManager.createQuery("select p from Project p where p.name = :name", Project.class);
        query.setParameter("name", name);
        return query.getSingleResult();
    }

    @Override
    public List<Project> findProjectsByUserId(int userId) {
        TypedQuery<Project> query = entityManager.createQuery("select p from Project p where p.userId = :userId", Project.class);
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    @Override
    public void updateProject(Project project) {
        entityManager.merge(project);
    }

    @Override
    public void deleteById(Integer id) throws SQLException {
        entityManager.remove(findOne(id));
    }

    @Override
    public void deleteByName(String name) throws Exception {
        entityManager.remove(findByName(name));
    }
}
