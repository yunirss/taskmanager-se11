package ru.suyundukov.tm.repository;

import ru.suyundukov.tm.entity.Task;

import java.sql.SQLException;
import java.util.List;

public interface TaskRepository {

    void save(Task task) throws SQLException;

    List<Task> findAll() throws SQLException;

    Task findOne(Integer id) throws SQLException;

    Task findByName(String name) throws SQLException;

    void deleteById(Integer id) throws SQLException;

    void deleteByName(String name) throws SQLException;

    void deleteTasksByProjectId(Integer id) throws SQLException;

    void updateTask(Task task) throws SQLException;

    List<Task> findTasksByProjectId(int projectId) throws SQLException;

    List<Task> findTasksByUserId(int userId) throws SQLException;
}
