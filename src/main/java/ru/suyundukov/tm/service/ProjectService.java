package ru.suyundukov.tm.service;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import lombok.AllArgsConstructor;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.repository.ProjectRepository;
import ru.suyundukov.tm.repository.impl.ProjectRepositoryImpl;

import java.util.List;

@AllArgsConstructor
public class ProjectService implements ProjectServiceInterface {

    private final EntityManagerFactory entityManagerFactory;

    @Override
    public void save(Project project) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final ProjectRepository projectRepository = new ProjectRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.save(project);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public List<Project> findAll() throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final ProjectRepository projectRepository = new ProjectRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        final List<Project> users = projectRepository.findAll();
        entityManager.close();
        return users;
    }

    public void updateProject(Project project) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final ProjectRepository projectRepository = new ProjectRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.updateProject(project);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public Project findById(Integer id) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final ProjectRepository projectRepository = new ProjectRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        Project project = projectRepository.findOne(id);
        entityManager.close();
        return project;
    }

    @Override
    public Project findByName(String name) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final ProjectRepository projectRepository = new ProjectRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        Project project = projectRepository.findByName(name);
        entityManager.close();
        return project;
    }

    public void deleteByName(String name) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final ProjectRepository projectRepository = new ProjectRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.deleteByName(name);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void deleteById(Integer id) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final ProjectRepository projectRepository = new ProjectRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.deleteById(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public boolean exist(Integer id) throws Exception {
        return findAll().isEmpty();
    }

    @Override
    public void load(Project[] projects) throws Exception {
        for (Project project : projects) {
            save(project);
        }
    }

    @Override
    public void load(List<Project> projectList) throws Exception {
        for (Project project : projectList) {
            save(project);
        }
    }
}
