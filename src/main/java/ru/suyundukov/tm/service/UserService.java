package ru.suyundukov.tm.service;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.repository.UserRepository;
import ru.suyundukov.tm.repository.impl.UserRepositoryImpl;

import java.util.List;

public class UserService implements UserServiceInterface {
    private final EntityManagerFactory entityManagerFactory;
    private User userOnline;

    public UserService(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public User getUserOnline() {
        return userOnline;
    }

    public void setUserOnline(User userOnline) {
        this.userOnline = userOnline;
    }

    @Override
    public void save(User user) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final UserRepository userRepository = new UserRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        userRepository.save(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public User findById(Integer id) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final UserRepository userRepository = new UserRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        final User user = userRepository.findById(id);
        entityManager.close();
        return user;
    }

    @Override
    public User findByName(String name) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final UserRepository userRepository = new UserRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        final User user = userRepository.findByName(name);
        entityManager.close();
        return user;
    }

    @Override
    public List<User> findAll() throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final UserRepository userRepository = new UserRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        List<User> users = userRepository.findAll();
        entityManager.close();
        return users;
    }

    @Override
    public void deleteById(Integer id) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final UserRepository userRepository = new UserRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        userRepository.deleteById(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public boolean exist(Integer id) throws Exception {
        return findAll().isEmpty();
    }

    @Override
    public void load(User[] users) throws Exception {
        for (User user : users) {
            save(user);
        }
    }

    @Override
    public void load(List<User> users) throws Exception {
        for (User user : users) {
            save(user);
        }
    }
}