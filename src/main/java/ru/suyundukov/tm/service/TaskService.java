package ru.suyundukov.tm.service;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import lombok.AllArgsConstructor;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.repository.TaskRepository;
import ru.suyundukov.tm.repository.impl.TaskRepositoryImpl;

import java.sql.SQLException;
import java.util.List;

@AllArgsConstructor
public class TaskService implements TaskServiceInterface {

    private final EntityManagerFactory entityManagerFactory;

    @Override
    public void save(Task task) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final TaskRepository taskRepository = new TaskRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.save(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public List<Task> findAll() throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final TaskRepository taskRepository = new TaskRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        final List<Task> tasks = taskRepository.findAll();
        entityManager.close();
        return tasks;
    }

    @Override
    public Task findById(Integer id) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final TaskRepository taskRepository = new TaskRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        final Task task = taskRepository.findOne(id);
        entityManager.close();
        return task;
    }

    @Override
    public Task findByName(String name) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final TaskRepository taskRepository = new TaskRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        final Task task = taskRepository.findByName(name);
        entityManager.close();
        return task;
    }

    public List<Task> findTasksByProjectId(int projectId) throws SQLException {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final TaskRepository taskRepository = new TaskRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        final List<Task> tasks = taskRepository.findTasksByProjectId(projectId);
        entityManager.close();
        return tasks;
    }

    @Override
    public List<Task> findTasksByUserId(int userId) throws SQLException {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final TaskRepository taskRepository = new TaskRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        final List<Task> tasks = taskRepository.findTasksByUserId(userId);
        entityManager.close();
        return tasks;
    }

    public void deleteTasksByProjectId(int id) throws Exception {
        for (Task task : findTasksByProjectId(id)) {
            deleteById(task.getId());
        }
    }

    @Override
    public void deleteByName(String name) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final TaskRepository taskRepository = new TaskRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.deleteByName(name);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void deleteById(Integer id) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final TaskRepository taskRepository = new TaskRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.deleteById(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void updateTask(Task task) throws Exception {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final TaskRepository taskRepository = new TaskRepositoryImpl(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.updateTask(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public boolean exist(Integer id) throws Exception {
        return findAll().isEmpty();
    }

    @Override
    public void load(Task[] tasks) throws Exception {
        for (Task task : tasks) {
            save(task);
        }
    }

    @Override
    public void load(List<Task> tasks) throws Exception {
        for (Task task : tasks) {
            save(task);
        }
    }
}