package ru.suyundukov.tm;

import java.sql.SQLException;

/**
 * Основной класс
 */
public class App {
    public static void main(String[] args) throws SQLException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }
}