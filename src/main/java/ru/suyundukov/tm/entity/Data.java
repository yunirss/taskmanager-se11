package ru.suyundukov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class Data implements Serializable {
    List<Project> projects = new ArrayList<>();
    List<Task> tasks = new ArrayList<>();
    List<User> users = new ArrayList<>();
}
