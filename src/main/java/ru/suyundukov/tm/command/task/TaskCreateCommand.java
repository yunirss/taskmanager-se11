package ru.suyundukov.tm.command.task;

import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.enums.Status;
import ru.suyundukov.tm.utils.ConsoleHelper;

import java.time.LocalDateTime;

public class TaskCreateCommand extends AbstractCommand {
    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATED]");
        System.out.println("[ENTER NAME:]");
        String name = ConsoleHelper.readString();
        System.out.println("[ENTER DESCRIPTION:]");
        String description = ConsoleHelper.readString();
        System.out.println("[ENTER NAME OF PROJECT:]");
        String projectName = ConsoleHelper.readString();
        Project project = serviceLocator.getProjectService().findByName(projectName);
        if (project.getId() != null) {
            final Task task = new Task();
            task.setName(name);
            task.setDescription(description);
            task.setCreateDate(LocalDateTime.now());
            task.setStatus(Status.OPEN);
            task.setUserId(serviceLocator.getUserService().getUserOnline().getId());
            task.setProjectId(project.getId());
            serviceLocator.getTaskService().save(task);
            System.out.println("[OK]");
        } else {
            System.out.println("[NO SUCH PROJECT EXIST]");
        }
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}